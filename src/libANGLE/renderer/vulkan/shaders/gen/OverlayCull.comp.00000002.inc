// GENERATED FILE - DO NOT EDIT.
// Generated by gen_vk_internal_shaders.py.
//
// Copyright 2018 The ANGLE Project Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
//
// shaders/gen/OverlayCull.comp.00000002.inc:
//   Pre-generated shader for the ANGLE Vulkan back-end.

#pragma once
constexpr uint8_t kOverlayCull_comp_00000002[] = {
    0x1f,0x8b,0x08,0x00,0x00,0x00,0x00,0x00,0x02,0xff,0x65,0x96,0x4b,0x6c,0x95,0x55,
    0x10,0xc7,0xcf,0xdc,0xdb,0x16,0x94,0x44,0x1e,0x15,0x2b,0xe5,0xda,0x40,0xaf,0x89,
    0x21,0x9a,0x82,0xa9,0x8f,0x5e,0xb5,0x4a,0x25,0xb6,0xbd,0xb5,0xb5,0xf4,0xf6,0x21,
    0xf6,0xb1,0x20,0x98,0x10,0x17,0xb2,0x91,0x9d,0x2e,0x4c,0x60,0x51,0x4c,0x24,0x71,
    0xe3,0xa6,0x1b,0x8a,0x25,0x3e,0x02,0xc6,0x85,0x4d,0x24,0x46,0x1b,0xb4,0x10,0xb5,
    0x26,0x46,0x16,0x56,0xd3,0x44,0x90,0x48,0x04,0x89,0x44,0x10,0xad,0x05,0x09,0xce,
    0xff,0x3b,0xff,0x69,0xa7,0x5f,0x9b,0x9c,0x7c,0xdf,0xfc,0x67,0xce,0xcc,0xef,0x3c,
    0xbe,0xe9,0xcd,0x66,0xf2,0xcb,0x42,0x90,0x70,0x7b,0x58,0x1e,0xf6,0x4b,0x48,0xfe,
    0x56,0x87,0x4c,0xc0,0xeb,0x8a,0x50,0x91,0x3c,0x5b,0xda,0xbb,0xdb,0xeb,0x5e,0xd9,
    0xfb,0x62,0xdd,0x43,0x0f,0x6f,0x81,0xff,0x8e,0x90,0x4d,0xe2,0xe0,0x5b,0x19,0x96,
    0x85,0x72,0x7d,0x96,0xe9,0x78,0x79,0xe7,0x4b,0x7b,0xa0,0xb7,0xea,0x18,0xd6,0xb1,
    0x4a,0xe7,0x97,0x25,0xf9,0xf0,0x1e,0xe3,0x93,0x7c,0xaa,0xb6,0x26,0xf9,0x43,0x58,
    0x4f,0xbb,0x4b,0x9f,0x15,0x8c,0x6b,0xd5,0x8c,0xa5,0x88,0x12,0xf2,0x7c,0xb6,0x68,
    0x4d,0x68,0x19,0xc6,0xf7,0xe8,0xb3,0x76,0xde,0x17,0xed,0x8d,0x2e,0xff,0x30,0xf3,
    0x57,0xd3,0x1e,0x4b,0xc5,0x8f,0x31,0xde,0x72,0xc3,0x5e,0x47,0xdf,0x31,0xce,0x85,
    0x5d,0xa9,0x15,0x33,0x49,0x6c,0x36,0x59,0x35,0xde,0xd7,0x6a,0x0c,0x58,0x37,0x70,
    0x7e,0x95,0xda,0x2b,0xc8,0x0f,0xff,0xfd,0xf4,0xaf,0xd1,0xb1,0x5c,0xc7,0x9d,0xaa,
    0x56,0x3a,0xbd,0x8a,0xeb,0xcc,0xa9,0x7d,0x37,0xe7,0x55,0x25,0xf9,0xca,0x92,0x9a,
    0xd8,0x33,0xe8,0x8f,0xd3,0xae,0xa6,0x06,0x7f,0x8e,0xef,0x15,0x2e,0x5f,0x9e,0x9a,
    0xd9,0x75,0x29,0x8e,0x02,0xf7,0x05,0x9c,0xcd,0x9c,0x9b,0x65,0xbe,0x16,0xfa,0x9a,
    0x59,0xaf,0x85,0xe7,0x27,0x6e,0x7e,0x91,0xeb,0x44,0x7c,0x1b,0x7d,0x15,0xcc,0xd7,
    0xc1,0x77,0x5f,0x7f,0x3b,0xf7,0x26,0xc7,0x73,0xed,0xa0,0x56,0xc3,0x33,0xec,0x62,
    0xae,0x6e,0x72,0x96,0x58,0x1b,0x76,0x8f,0xdb,0xe3,0x5e,0xe6,0x31,0x16,0xd8,0x7d,
    0x8e,0xe5,0x05,0xc6,0x76,0x90,0x65,0x77,0x72,0x5e,0x51,0x7b,0x40,0xef,0x10,0xce,
    0x64,0x2f,0xf9,0x8b,0x6e,0x7d,0xc3,0xcc,0x69,0x31,0x6f,0x72,0x0f,0xf3,0xc9,0x99,
    0xdf,0x16,0x0e,0x73,0x4d,0xfc,0x1c,0xe6,0xff,0x32,0xc9,0x1a,0x62,0xed,0x77,0xa8,
    0x1d,0x66,0x5e,0xd8,0x63,0xee,0x3e,0xbc,0xab,0xcf,0xde,0x79,0x96,0x8a,0x64,0x7f,
    0x8f,0xf1,0x9c,0x0b,0xf3,0x67,0x92,0x4d,0xea,0xef,0xd3,0x42,0x8f,0x28,0x4b,0x86,
    0xfb,0x18,0x78,0x3e,0xff,0xaa,0x82,0x6f,0xab,0x91,0xe7,0x76,0x80,0xec,0x9d,0xfa,
    0xcd,0x61,0xde,0x1b,0xd4,0x0e,0x70,0x0e,0x78,0xf7,0x73,0x4d,0x07,0xe9,0xc7,0xda,
    0x5e,0xa7,0xf6,0x16,0x75,0x68,0x4d,0xaa,0xe1,0x2c,0x3f,0xe6,0x79,0x17,0x59,0x07,
    0xeb,0x1e,0xa7,0xde,0xa4,0x16,0xf6,0xf8,0x13,0x9e,0x4b,0x1f,0x7d,0x8d,0x3c,0xf7,
    0xe3,0xf4,0x75,0x69,0x2e,0xcc,0xfb,0x94,0x5a,0x70,0xda,0x67,0xd4,0xb0,0xb6,0x8f,
    0x54,0xc3,0xf9,0x7c,0xce,0x58,0xf8,0x66,0x75,0xa5,0x93,0x9c,0x73,0x5d,0xf3,0xc2,
    0x37,0xa1,0x63,0x92,0xeb,0x9f,0x70,0xeb,0x3d,0xc1,0x5c,0xc7,0xdd,0x7a,0x91,0x13,
    0xe7,0xfe,0x05,0xfd,0x58,0xe3,0x88,0xe6,0x41,0x9d,0x2f,0xa9,0xcf,0x69,0x1e,0xcb,
    0x87,0xe7,0xdf,0x9a,0x0f,0xfe,0x93,0x64,0x29,0x67,0xec,0x04,0x79,0xa6,0x1c,0x0f,
    0x62,0x4e,0xe9,0x98,0xe2,0xfc,0x53,0x8e,0xe7,0x2b,0xc7,0x93,0xe1,0x99,0x7d,0x48,
    0x9e,0xaf,0xe9,0x3f,0xe8,0x78,0xbe,0xa1,0x0e,0x1e,0xcb,0x37,0xe5,0x78,0xbe,0x25,
    0xd3,0x24,0x63,0x51,0xeb,0x7d,0xdd,0x45,0xec,0xe3,0x77,0xf4,0x17,0x78,0x56,0x93,
    0xdc,0xdf,0xef,0xe9,0x1b,0xe7,0x99,0xa2,0x3f,0x9c,0x66,0xcf,0x80,0xf6,0xa4,0x52,
    0x9d,0x66,0x9c,0xed,0xff,0xcf,0xf4,0xad,0xe1,0x7a,0xcb,0x65,0x61,0xbd,0xf0,0x9d,
    0xc1,0x9e,0x48,0xe4,0x3b,0xc3,0xfb,0x83,0x5a,0x67,0xdd,0x3c,0xab,0xf5,0x0b,0x6b,
    0x9d,0x75,0xf7,0xe7,0x1c,0x75,0xb3,0x7f,0x25,0xd3,0x49,0xe6,0x39,0x4f,0xed,0x9c,
    0xe3,0x3b,0xef,0xf8,0x7e,0x63,0x9d,0x3c,0xf9,0xca,0x1c,0x1f,0x7c,0x17,0xf0,0x9d,
    0x90,0xef,0x82,0xe3,0xbb,0xe8,0xe6,0x19,0xdf,0xef,0xe4,0xbb,0xe8,0x78,0x2e,0x51,
    0x37,0xfb,0x8f,0x14,0xdf,0x65,0x6a,0x97,0x1c,0xdf,0x65,0xc7,0xf7,0x27,0xeb,0xd4,
    0x91,0x2f,0xeb,0xf8,0xe0,0xbb,0x82,0xbb,0x40,0xbe,0x2b,0x8e,0xef,0xaa,0x9b,0x67,
    0x7c,0x7f,0x91,0xef,0xaa,0xe3,0xb9,0x46,0xdd,0xec,0x7f,0x52,0x7c,0xb3,0xd4,0xae,
    0x39,0xbe,0x59,0xc7,0x37,0xc7,0x3a,0x05,0xf2,0x65,0x1c,0xdf,0x5c,0xf2,0x0c,0x89,
    0x06,0xbe,0xeb,0x8e,0xef,0x86,0x9b,0x67,0x7c,0xff,0x91,0xef,0x86,0xe3,0xb9,0x49,
    0xdd,0xec,0x5b,0x29,0x3e,0x7c,0x98,0xb7,0x18,0x67,0x7c,0xd0,0x70,0xef,0xad,0x6e,
    0x86,0xb6,0xed,0x53,0x96,0xb6,0x9d,0x6b,0x19,0x6d,0xbb,0x87,0x78,0x7e,0xc0,0xf5,
    0xfd,0x40,0xce,0x22,0xd7,0x37,0xe3,0xbe,0x57,0xf8,0xa6,0x75,0xcc,0xf0,0xfb,0x9a,
    0x76,0x6b,0xf9,0x91,0x6b,0xe9,0x73,0xec,0x3f,0x51,0x2f,0xa9,0x85,0x6f,0x7a,0xa7,
    0x44,0x6d,0x1f,0xf7,0x0c,0x0c,0x96,0x6b,0x86,0xdf,0x2a,0xe2,0xf6,0x48,0xfc,0xdf,
    0x02,0x2e,0xcc,0x99,0x76,0x3d,0xef,0x6d,0xfa,0x83,0xdb,0xdb,0x95,0x12,0x7b,0xff,
    0xb8,0xeb,0xab,0xab,0x64,0xa1,0xaf,0xc2,0x6f,0x7d,0x75,0xb5,0x44,0x9f,0xe5,0xab,
    0x94,0xa8,0xf9,0xbe,0xba,0x96,0x9a,0xef,0xab,0x77,0x49,0x8c,0x85,0x0f,0xfb,0x92,
    0x73,0xe7,0x0e,0x5f,0x95,0x8e,0x1c,0xf7,0x13,0xef,0xd6,0xc7,0xd6,0x31,0x97,0xd5,
    0xf0,0x7d,0xb5,0x5a,0xa2,0xdf,0xf7,0xd5,0xf5,0x12,0x75,0xec,0x8d,0xe5,0xc3,0xd3,
    0xfa,0xd8,0x3d,0x12,0x59,0x66,0x18,0x5b,0x45,0x9e,0xbc,0xe3,0x41,0x4c,0x8d,0x8e,
    0x3c,0xe7,0xd7,0x38,0x9e,0x0d,0x8e,0x27,0xdd,0x57,0x37,0x4a,0xf4,0xfb,0xbe,0x5a,
    0x2b,0x51,0x07,0x8f,0xe5,0xcb,0x3b,0x9e,0x7b,0x25,0x32,0x81,0xb1,0x96,0x75,0xad,
    0xaf,0xde,0x27,0xd1,0x9f,0xee,0xab,0x9b,0x24,0xfa,0x7c,0x0f,0xdd,0xc4,0x75,0x0c,
    0xa5,0xfa,0xe5,0xa3,0x6a,0x0f,0xb1,0x2e,0xde,0xed,0xcc,0x1b,0x64,0x69,0xbf,0x2c,
    0x48,0xbc,0x83,0x0d,0xb2,0x70,0x07,0x1f,0x93,0xa8,0x9b,0xfd,0x84,0x2c,0xfe,0x9e,
    0x1a,0x25,0x6a,0x88,0x33,0x16,0x68,0x76,0xee,0x4f,0xc9,0xe2,0x7e,0x39,0xe8,0xf8,
    0xe0,0xdb,0xaa,0x63,0x90,0x7c,0x5b,0x1d,0x5f,0x93,0x2c,0xed,0x97,0x4f,0x93,0xaf,
    0xc9,0xf1,0x6c,0x93,0xa8,0x9b,0xfd,0x4c,0x8a,0xaf,0x59,0xa2,0xb6,0xcd,0xf1,0x35,
    0x3b,0xbe,0x56,0x59,0xdc,0x2f,0x07,0x1c,0x1f,0x7c,0x45,0x1d,0x03,0xe4,0x2b,0x3a,
    0xbe,0x36,0x59,0xda,0x2f,0x9f,0x25,0x5f,0x9b,0xe3,0x69,0x97,0xa8,0x9b,0xfd,0x5c,
    0x8a,0xaf,0x53,0xa2,0xd6,0xee,0xf8,0x3a,0x1d,0x5f,0x97,0x2c,0xee,0x97,0xfd,0x8e,
    0x0f,0xbe,0x92,0x8e,0x7e,0xf2,0x95,0x1c,0x5f,0xb7,0x2c,0xed,0x97,0x3d,0xe4,0xeb,
    0x76,0x3c,0xbd,0x12,0x75,0xb3,0x9f,0x4f,0xf1,0xed,0x90,0xa8,0xf5,0x3a,0xbe,0x1d,
    0xbc,0xcf,0x56,0xb7,0x9f,0xb6,0xed,0xd3,0x00,0x6d,0x3b,0xd7,0x41,0xda,0x76,0x0f,
    0x87,0x78,0x57,0xeb,0x65,0x71,0x6f,0xdc,0xac,0x76,0x3d,0x63,0xf0,0x6e,0xdc,0x5b,
    0x64,0x69,0x6f,0x7c,0x50,0xa2,0x6e,0xbd,0x71,0xb7,0x44,0xcd,0xf7,0x46,0xcb,0x55,
    0x2f,0x0b,0xbd,0xf1,0x55,0xf6,0x46,0x30,0x60,0xce,0x66,0xd7,0xcb,0x46,0xe8,0x0f,
    0xdc,0xeb,0xd1,0x54,0xef,0x3e,0xa4,0x63,0x94,0xfd,0xf6,0x10,0x59,0xf0,0x1b,0xfb,
    0x08,0x7f,0x5f,0xbf,0xc6,0xdf,0xd6,0xef,0xf1,0xf7,0xed,0x76,0xad,0x89,0x9e,0x79,
    0x94,0x7d,0x77,0xc4,0xfd,0xde,0xdf,0xa5,0xb1,0x47,0x18,0x7b,0x94,0xbc,0x96,0x7b,
    0x34,0xf9,0xff,0x24,0xa1,0x41,0xc7,0xff,0x7e,0xc7,0xe2,0x37,0x04,0x0f,0x00,0x00
};

// Generated from:
//
// #version 450 core
//
// #extension GL_EXT_samplerless_texture_functions : require
//
// layout(local_size_x = 16, local_size_y = 1, local_size_z = 1)in;
//
// layout(set = 0, binding = 0, rgba32ui)uniform writeonly uimage2D culledWidgetsOut;
//
// layout(set = 0, binding = 1)uniform WidgetCoordinates
// {
//     uvec4 coordinates[16 + 16];
// };
//
// shared uint intersectingWidgets[16];
//
// void accumulateWidgets(const uint localId)
// {
//     if(localId < 8)
//     {
//         intersectingWidgets[localId]|= intersectingWidgets[localId + 8];
//         if(localId < 4)
//         {
//             intersectingWidgets[localId]|= intersectingWidgets[localId + 4];
//             if(localId < 2)
//             {
//                 intersectingWidgets[localId]|= intersectingWidgets[localId + 2];
//                 if(localId < 1)
//                 {
//                     intersectingWidgets[localId]|= intersectingWidgets[localId + 1];
//                 }
//             }
//         }
//     }
// }
//
// uvec2 cullWidgets(const uint offset, const uvec2 blockCoordLow, const uvec2 blockCoordHigh)
// {
//     const uint localId = gl_LocalInvocationID . x;
//     const uvec4 widgetCoords = coordinates[offset + localId];
//
//     const bool intersects = widgetCoords . x < widgetCoords . z &&
//                             all(lessThan(widgetCoords . xy, blockCoordHigh))&&
//                             all(greaterThanEqual(widgetCoords . zw, blockCoordLow));
//
//     uvec2 ballot = uvec2(0, 0);
//
//     intersectingWidgets[localId]= uint(intersects)<< localId;
//     accumulateWidgets(localId);
//     if(localId == 0)
//     {
//         ballot . x = intersectingWidgets[0];
//     }
//
//     return ballot;
// }
//
// void main()
// {
//
//     const uvec2 outCoord = gl_WorkGroupID . xy;
//     const uvec2 blockCoordLow = outCoord * uvec2(4, 4);
//     const uvec2 blockCoordHigh = blockCoordLow + uvec2(4, 4);
//
//     uvec2 culledWidgets;
//
//     culledWidgets . x = cullWidgets(0, blockCoordLow, blockCoordHigh). x;
//     culledWidgets . y = cullWidgets(16, blockCoordLow, blockCoordHigh). x;
//
//     if(gl_LocalInvocationID . x == 0)
//     {
//         imageStore(culledWidgetsOut, ivec2(outCoord), uvec4(culledWidgets, 0, 0));
//     }
// }
